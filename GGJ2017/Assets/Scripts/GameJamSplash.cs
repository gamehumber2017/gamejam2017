﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameJamSplash : MonoBehaviour {

    private void OnEnable()
    {
        StartCoroutine(ChangeScene());
    }

    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(5.0f);
        Debug.Log("changin");
        SceneManager.LoadScene("MainMenu");
        yield return null;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadingScene : MonoBehaviour {

    public Texture2D textureForFading;
    public float fadeSpeed = 1.5f;

    private int drawDepth = -1000;
    private float textureAlpha = 1.0f;
    public int fadeDirection = -1;

    private void OnGUI()
    {

        textureAlpha += fadeDirection * fadeSpeed * Time.deltaTime;
        textureAlpha = Mathf.Clamp01(textureAlpha);

        GUI.color = new Color(GUI.color.r, GUI.color.g, GUI.color.b, textureAlpha);
        GUI.depth = drawDepth;
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), textureForFading);

    }
    

    // Use this for initialization
    private void Awake()
    {
        
    }
    void Start ()
    {

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}

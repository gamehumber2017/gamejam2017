﻿using UnityEngine.UI;
using UnityEngine;

public class EnergyManager : MonoBehaviour {

    public enum GoalsEnum
    {
        Bathroom,
        Food,
        Beer,
        Stage
    }

    public float MaxEnergy = 1.0f;
    public float MinEnergy = 0.0f;
    public float CurrEnergy = 0.7f;
    public float DecayRate = 0.0f;

    public Slider slider;

    public GameObject GameOverPanel;

    private static EnergyManager m_Instance;
    public static EnergyManager GetInstance()
    {
        if (!m_Instance)
        {
            m_Instance = FindObjectOfType<EnergyManager>();
            if (!m_Instance)
            {
                GameObject newSpawnManager = new GameObject("EnergyManager");
                m_Instance = newSpawnManager.AddComponent<EnergyManager>();
            }
        }
        return m_Instance;
    }

    private void Start()
    {
        slider.value = CurrEnergy;
    }

    private void Update()
    {
        //slider.value -= DecayRate * Time.deltaTime;

        if(slider.value <= 0.0f)
        {
            // switch to you lose scene

            GameOverPanel.SetActive(true);
            InputManager.GetInstance().HasLost = true;
        }
        ChangeEnergy(0.0075f * Time.deltaTime);

    }

    // add the amount to the current evergy,
    // if the amount is negative, then it will decrease to the energy.
    public void ChangeEnergy(float amout)
    {
        slider.value += amout;

        if (slider.value >= 1.0f)
        {
            CrowdManager.GetInstance().CrowdGoCrazy();
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierHandler : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        PersonBehaviour p = other.gameObject.GetComponent<PersonBehaviour>();

        if(p && p.bIsRidingCrowd)
        {
            Destroy(other.gameObject);
        }
    }
}

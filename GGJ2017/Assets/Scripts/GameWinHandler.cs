﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameWinHandler : MonoBehaviour {

    public Animator AnimatorController;

    private void OnEnable()
    {
        AnimatorController.SetTrigger("GameOver");

        InputManager.GetInstance().SetEnabledInput(false);
    }

    public void RestartLevel()
    {
        InputManager.GetInstance().SetEnabledInput(true);
        SceneManager.LoadScene("Level_One");
    }

    public void QuitLevel()
    {
        InputManager.GetInstance().SetEnabledInput(true);
        SceneManager.LoadScene("MainMenu");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class Floor : MonoBehaviour {

    private void OnCollisionEnter(Collision collision)
    {
        PersonBehaviour p = collision.gameObject.GetComponent<PersonBehaviour>();

        if(p)
        {
            if(p.bIsRidingCrowd)
            {
                EnergyManager.GetInstance().ChangeEnergy(-0.05f);
                Destroy(collision.gameObject);
            }
        }
    }
}

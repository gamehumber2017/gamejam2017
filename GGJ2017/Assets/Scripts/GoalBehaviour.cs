﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public class GoalBehaviour : MonoBehaviour {

    public enum GoalsEnum
    {
        Food,
        Stage,
        Drinks,
        Bathroom,
    }

    public GoalsEnum GoalType;

    private void OnTriggerEnter(Collider other)
    {
        PersonBehaviour personBehaviour = other.gameObject.GetComponent<PersonBehaviour>();

        if(personBehaviour && personBehaviour.bIsRidingCrowd) 
        {
            if(personBehaviour.PersonNeed == GoalType)
            {
                CrowdManager.GetInstance().RemovePersonWithNeedsCount();
                EnergyManager.GetInstance().ChangeEnergy(0.25f);
                Destroy(other.gameObject);
            }
        }
    }
}

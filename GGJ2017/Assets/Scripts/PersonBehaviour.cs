﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonBehaviour : MonoBehaviour {

    public int Type;
    public GoalBehaviour.GoalsEnum PersonNeed;

    private Rigidbody rb;
    public bool bIsGrounded;
    public bool bIsWaving;

    public bool bIsRidingCrowd = false;
    bool bIsNeedy;
    float GetToGoalTime = 30.0f;
    float GetToGoalTimer;

    float ridingRandomForce = 10.0f;

    public bool bIsMoshing;
    Vector3 originalposition;

    float MoshForce;
    public PersonBehaviour MoshInstigator;
    float MoshTimer;
    float MoshLength;

    public Sprite AngerImage;
    public Sprite[] NeedsImages;
    PersonBubble Bubble;

    bool bIsActingCrazy;
    float CrazyTimer;
    float CrazyTimeLength;

    bool bIsFinished;

    public GameObject moshLightPrefab;

    private void Awake()
    {
        CrazyTimeLength = 7.0f;
        MoshLength = 10.0f;
        rb = GetComponent<Rigidbody>();
        bIsGrounded = false;
        rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
    }

    private void FixedUpdate()
    {
        if (bIsActingCrazy)
            GoCrazy();

        if (!bIsMoshing)
        {
            //if (bIsGrounded && !bIsWaving && !bIsNeedy)
            if (bIsGrounded && !bIsWaving)
            {


                //else
                //rb.AddForce(Vector3.up * Random.Range(2f, 6f), ForceMode.VelocityChange);
                rb.AddTorque((gameObject.transform.up + gameObject.transform.right) * Random.Range(-6f, 6f), ForceMode.VelocityChange);
                rb.AddForce(Vector3.up * Random.Range(1.0f, 1.75f), ForceMode.VelocityChange);
            }
            else
            {
                if (bIsRidingCrowd)
                {
                    float x = Random.Range(-1.0f, 1.0f);
                    float y = 0;
                    float z = Random.Range(-1.0f, 1.0f);
                    Vector3 forceDirection = new Vector3(x, y, z);
                    rb.AddForce(forceDirection * ridingRandomForce, ForceMode.Force);

                    if(!bIsFinished)
                    { 
                        GetToGoalTimer += Time.fixedDeltaTime;
                        if(GetToGoalTimer>GetToGoalTime)
                        {
                            if (!Bubble)
                            {
                                GameObject BubbleObject = new GameObject("Needs Bubble");
                                Bubble = BubbleObject.AddComponent<PersonBubble>();
                            }
                            Bubble.LoadNewImage(AngerImage, Color.red);

                            EnergyManager.GetInstance().ChangeEnergy(-0.05f);
                            StartCoroutine(OnMadDeath());
                            bIsFinished = true;
                        }
                    }
                }
            }
                
        }
        else
        {
            if(bIsGrounded)
            {
                { 
                    MoshForce = Random.Range(4.0f,7.0f);
                    //rb.AddForce(Vector3.up * MoshForce, ForceMode.VelocityChange);
                    rb.velocity = Vector3.up * MoshForce;
                }
            }

            MoshTimer += Time.fixedDeltaTime;
            if(MoshTimer>MoshLength + 2)
            {
                bIsMoshing = false;
            }

        }


    }

    public void BecomeNeedy(int row, int col)
    {
        if (bIsMoshing || bIsActingCrazy)
            return;

        originalposition = gameObject.transform.position;
        //bIsNeedy = true;
        bIsRidingCrowd = true;
        rb.constraints = RigidbodyConstraints.None;
        rb.AddForce(Vector3.up * 10, ForceMode.VelocityChange);

        //Renderer matRen = GetComponent<Renderer>();
        //Color color = matRen.material.color;
        //matRen.material.EnableKeyword("_EMISSION");
        //matRen.material.SetColor("_EmissionColor", color * 10.0f);

        GameObject BubbleObject = new GameObject("Needs Bubble");
        Bubble = BubbleObject.AddComponent<PersonBubble>();

        int needchoice = Random.Range(0, 3);

        switch(needchoice)
        {
            case 0:
                PersonNeed = GoalBehaviour.GoalsEnum.Bathroom;
                Bubble.LoadNewImage(NeedsImages[needchoice], Color.blue);
                break;
            case 1:
                PersonNeed = GoalBehaviour.GoalsEnum.Drinks;
                Bubble.LoadNewImage(NeedsImages[needchoice], Color.green);
                break;
            case 2:
                PersonNeed = GoalBehaviour.GoalsEnum.Food;
                Bubble.LoadNewImage(NeedsImages[needchoice], Color.yellow);
                break;
            default: break;
        }

        
        Bubble.transform.localScale *= 0.15f;
        Bubble.transform.position = transform.position;
        Bubble.transform.position += new Vector3(0, 2, 0);
        Bubble.transform.SetParent(gameObject.transform,true);
        Bubble.gameObject.SetActive(true);

        foreach(Renderer r in GetComponentsInChildren<Renderer>() ){
            Color color = r.material.color;
            r.material.EnableKeyword("_EMISSION");
            r.material.SetColor("_EmissionColor", color * 10.0f);
        }

        SpawnManager.GetInstance().CreateNewPerson(row, col);
    }

    public void StartMosh()
    {
        if(!bIsRidingCrowd)
        {
            bIsMoshing = true;

            GameObject moshLight = (GameObject)Instantiate(moshLightPrefab);
            moshLight.transform.position = transform.position + new Vector3(0f, 15.0f, 0f);
            Collider[] otherPeople =  Physics.OverlapSphere(transform.position, 5.0f);

            StartCoroutine(destroyLight(moshLight));

            foreach (Collider otherPerson in otherPeople)
            {
                PersonBehaviour other = otherPerson.GetComponent<PersonBehaviour>();
                if(other && other != this)
                {
                    other.MoshInstigator = this;
                    other.bIsMoshing = true;
                }
            }
        }
    }

    public void GoCrazy()
    {
        if (!bIsActingCrazy)
        { 
            bIsActingCrazy = true;
        }
        else
            CrazyTimer += Time.fixedDeltaTime;

        if (CrazyTimer > CrazyTimeLength)
        {
            bIsActingCrazy = false;
        }

        if(bIsGrounded)
        {
            rb.velocity = Vector3.up * 3.5f;
        }

    }

    public void DoAWave(float WaveStrength, Vector3 WaveDirection)
    {
        if (!bIsWaving && !bIsMoshing)
        {
            bIsWaving = true;
            if (rb.isKinematic)
                rb.isKinematic = false;
            //Vector3 pos = rb.position;
            //pos.y = 0;
            //rb.position = new Vector3(rb.position.x, 0.0f, rb.position.z);
            rb.velocity = Vector3.up * WaveStrength;
        }

        if (bIsRidingCrowd)
        {
            rb.velocity = WaveDirection * (WaveStrength/1.5f);
        }
    }

    IEnumerator OnMadDeath()
    {
        yield return new WaitForSeconds(5);
        CrowdManager.GetInstance().RemovePersonWithNeedsCount();
        Destroy(gameObject);

        yield return null;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            if(bIsRidingCrowd)
            {
                if(!bIsFinished)
                { 
                    if (!bIsGrounded)
                    { 
                        if(Bubble)
                            Bubble.LoadNewImage(AngerImage, Color.red);

                        EnergyManager.GetInstance().ChangeEnergy(-0.05f);
                        StartCoroutine(OnMadDeath());
                    }
                }
            }
            else
            { 
                bIsWaving = false;
            }
            bIsGrounded = true;
        }

        if(bIsMoshing)
        { 
            PersonBehaviour otherPerson = collision.gameObject.GetComponent<PersonBehaviour>();
            if (otherPerson)
            {
                if(otherPerson.bIsRidingCrowd)
                {
                    Rigidbody otherRB = otherPerson.GetComponent<Rigidbody>();
                    if(otherRB)
                    { 
                        if(MoshInstigator)
                        { 
                            otherRB.AddForce(((MoshInstigator.transform.position - otherPerson.transform.position) + (Vector3.up * 5)).normalized * (MoshForce/4),ForceMode.VelocityChange);
                        }
                    }
                }
            }
        }
    }

    private void LeaveConcert()
    {

    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            bIsGrounded = false;
        }
        else
        {
            //if(bIsNeedy)
            //{ 
                /*if(!bIsRidingCrowd)
                {
                    bIsRidingCrowd = true;
                    SpawnManager.GetInstance().AddSurfingPeople(this);
                    //rb.MoveRotation(Quaternion.Euler(0, Random.Range(-90,90), 90));
                    rb.angularVelocity = Random.insideUnitSphere * 2;
                    rb.AddForce(transform.right * 1.25f, ForceMode.VelocityChange);
                }*/
             //}
        }
    }

    IEnumerator destroyLight(GameObject light)
    {
        yield return new WaitForSeconds(MoshLength);
        Destroy(light);
    }
}

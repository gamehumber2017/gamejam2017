﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdManager : MonoBehaviour {

    // Use this for initialization

    int previousRow;
    int previousColumn;

    float SpawnPersonWithNeedsTimer;
    float SpawnPersonWithNeedsRate;

    int numOfNeeders;

    public bool bAllowMoshing;
    float MoshStartTime;
    float MoshStartTimer;


    private void Awake()
    {
        SpawnPersonWithNeedsRate = 5.0f;
        MoshStartTime = 10.0f;
    }

    void Start () {
		
	}

    private static CrowdManager m_Instance;
    public static CrowdManager GetInstance()
    {
        if(!m_Instance)
        {
            m_Instance = FindObjectOfType<CrowdManager>();
            if(!m_Instance)
            {
                GameObject newCrowdManager = new GameObject("Crowd Manager");
                m_Instance = newCrowdManager.AddComponent<CrowdManager>();
            }
        }
        return m_Instance;
    }
    public void RemovePersonWithNeedsCount()
    {
        numOfNeeders--;
        if (numOfNeeders < 0)
            numOfNeeders = 0;
    }
    public void CrowdGoCrazy()
    {
        GameObject[,] AllPeople = SpawnManager.GetInstance().AllPeople;

        for (int i = 0; i < AllPeople.GetLength(0); ++i)
        {
            for (int j = 0; j < AllPeople.GetLength(1); j++)
            {
                AllPeople[i, j].GetComponent<PersonBehaviour>().GoCrazy();
            }
        }
    }
    public void SpawnPersonWithNeeds()
    {
        int offset = 5;
        int r = Random.Range(offset, SpawnManager.GetInstance().NumRows- offset);
        int c = Random.Range(offset, SpawnManager.GetInstance().NumCols- offset);

        GameObject currentPerson = SpawnManager.GetInstance().AllPeople[r, c];

        if (currentPerson)
        {
            currentPerson.GetComponent<PersonBehaviour>().BecomeNeedy(r, c);
            numOfNeeders++;
            SpawnManager.GetInstance().AddSurfingPeople(currentPerson.GetComponent<PersonBehaviour>());
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(numOfNeeders<5)
        { 
            SpawnPersonWithNeedsTimer += Time.deltaTime;
            if (SpawnPersonWithNeedsTimer > SpawnPersonWithNeedsRate)
            {
                SpawnPersonWithNeeds();
                SpawnPersonWithNeedsTimer = 0.0f;
                SpawnPersonWithNeedsRate = Random.Range(7.0f, 10.0f);
            }
        }

        if (bAllowMoshing)
        {
            MoshStartTimer += Time.deltaTime;
            if(MoshStartTimer>MoshStartTime)
            {
                StartAMoshPit();
                MoshStartTimer = 0;
            }
        }
    }
    void StartAMoshPit()
    {
        int r = Random.Range(0, SpawnManager.GetInstance().NumRows);
        int c = Random.Range(0, SpawnManager.GetInstance().NumCols);
        GameObject currentPerson = SpawnManager.GetInstance().AllPeople[r, c];

        if (currentPerson)
        {
            currentPerson.GetComponent<PersonBehaviour>().StartMosh();
        }
    }
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChanger : MonoBehaviour {

    public AudioSource BandSource;

    public GameObject WinScreen;

    private static LevelChanger m_Instance;
    public static LevelChanger GetInstance()
    {
        if (!m_Instance)
        {
            m_Instance = FindObjectOfType<LevelChanger>();
            if (!m_Instance)
            {
                GameObject newCrowdManager = new GameObject("Level Changer");
                m_Instance = newCrowdManager.AddComponent<LevelChanger>();
            }
        }
        return m_Instance;
    }

    private void Update()
    {
        if(!BandSource.isPlaying && !InputManager.GetInstance().InGameMenuToggle && !InputManager.GetInstance().HasLost)
        {
            switch(SceneManager.GetActiveScene().name)
            {
                case "Level_One":
                    SceneManager.LoadScene("Level_Two");
                    break;
                case "Level_Two":
                    SceneManager.LoadScene("Level_Three");
                    break;
                case "Level_Three":
                    WinScreen.SetActive(true);
                    break;
                default:
                    break;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Speaker : MonoBehaviour {


    private static Speaker m_Instance;
    public List<AudioClip> Songs = new List<AudioClip>();
    AudioSource MusicSource;
    public int startIndex;

    public static Speaker GetInstance()
    {
        if(!m_Instance)
        {
            m_Instance = FindObjectOfType<Speaker>();
        }
        return m_Instance;
    }

    private void Awake()
    {
        MusicSource = GetComponent<AudioSource>();
        if(!MusicSource)
        {
            MusicSource = gameObject.AddComponent<AudioSource>();
        }
    }

    public void PlayTrackNum(int Index)
    {
        if (MusicSource.isPlaying)
            MusicSource.Stop();

        MusicSource.clip = Songs[Index];
        MusicSource.Play();
    }

    public AudioClip GetCurrentAudioTrack()
    {
        return MusicSource.clip;
    }


    // Use this for initialization
    void Start () {
        PlayTrackNum(startIndex);
	}
	
	// Update is called once per frame
	void Update () {


		
	}
}

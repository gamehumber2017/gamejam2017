﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingLight : MonoBehaviour
{
    public float TurnSpeed = 5.0f;
    public bool Cyclical;
    public Transform[] LookAtPositions;

    [SerializeField]
    private int CurrentTarget;
    private Vector3 lookTarget;

    private void Start()
    {
        CurrentTarget = 0;
        lookTarget = LookAtPositions[CurrentTarget].position;
        //StartCoroutine(RotateTowards(LookAtPositions[CurrentTarget].position));
    }

    private void Update()
    {
        if(CurrentTarget < LookAtPositions.Length)
        {
            Vector3 lookDirection = (lookTarget - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(lookDirection);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, 0.05f);
            
            if (Vector3.Dot(lookDirection, transform.forward) > 0.97f)
            {
                CurrentTarget++;

                if(CurrentTarget >= LookAtPositions.Length && Cyclical)
                {
                    CurrentTarget = 0;
                }
                lookTarget = LookAtPositions[CurrentTarget].position;
            }
        }
    }

    private IEnumerator RotateTowards(Vector3 lookAt)
    {
        
        yield return null;

        //while (CurrentTarget < LookAtPositions.Length)
        //{
            

        //    if (Vector3.Dot(lookDirection, transform.forward) > 0.9)
        //    {


        //        CurrentTarget++;

        //        if(CurrentTarget >= LookAtPositions.Length && Cyclical)
        //        {
        //            CurrentTarget = 0;
        //        }
        //    }

            
        //}

        //yield return null;
        /*Vector3 lookDirection = (to - transform.position).normalized;
        Debug.Log(Vector3.Dot(lookDirection, transform.forward) + " target " + CurrentTarget );

        // 90% looking at the target
        if(Vector3.Dot(lookDirection, transform.forward) > 0.9)
        {
            CurrentTarget++;
            if(CurrentTarget >= LookAtPositions.Length && Cyclical)
            {
                CurrentTarget = 0;
            } else
            {
                yield return null;
            }
        }

        Quaternion lookRotation = Quaternion.LookRotation(lookDirection);

        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * TurnSpeed);

        StartCoroutine(RotateTowards(LookAtPositions[CurrentTarget].position));*/
    }
}

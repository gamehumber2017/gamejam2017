﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrobeLight : MonoBehaviour {

    public Light mLight;

    public float waitingTime = 0.05f;
    public float offset = 0.0f;

    private void Start()
    {
        mLight.enabled = false;
        StartCoroutine(StartOffset());
    }

    IEnumerator StartStrobe()
    {
        while (true)
        {
            mLight.enabled = !(mLight.enabled); //toggle on/off the enabled property
            yield return new WaitForSeconds(waitingTime);
        }
    }

    IEnumerator StartOffset()
    {
        yield return new WaitForSeconds(offset);
        StartCoroutine(StartStrobe());
    }
}

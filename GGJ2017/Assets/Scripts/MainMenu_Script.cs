﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu_Script : MonoBehaviour {

    public Canvas MainMenuUI;
    public GameObject MenuPanel;
    public GameObject OptionsPanel;
    public GameObject LevelPanel;
    public GameObject HowToPlayPanel;
    public GameObject ExitPanel;
    public GameObject Credits;

    public AudioSource music;

    public AudioClip[] ButtonsHoverAudios;

    int selectedScene = 1;

    //we need a fade in/fade out or at least a fade to black to occur when the level starts

	// Use this for initialization
	void Start () {
        FindObjectOfType<FadingScene>().fadeDirection = -1;
        OptionsPanel.SetActive(false);
        ExitPanel.SetActive(false);
        HowToPlayPanel.SetActive(false);
        LevelPanel.SetActive(false);
        Credits.SetActive(false);
    }

    public void PlayGame()
    {
        StartCoroutine(FadeMusic());
    }

    public void LevelOne()
    {
        selectedScene = 1;
        StartCoroutine(FadeMusic());
    }

    public void LevelTwo()
    {
        selectedScene = 2;
        StartCoroutine(FadeMusic());
    }

    public void LevelThree()
    {
        selectedScene = 3;
        StartCoroutine(FadeMusic());
    }

    public void LevelSelect()
    {
        MenuPanel.SetActive(false);
        LevelPanel.SetActive(true);
        OptionsPanel.SetActive(false);
        HowToPlayPanel.SetActive(false);
        Credits.SetActive(false);
    }

    public void OpenOptions()
    {
        MenuPanel.SetActive(false);
        LevelPanel.SetActive(false);
        HowToPlayPanel.SetActive(false);
        OptionsPanel.SetActive(true);
        Credits.SetActive(false);
    }

    public void OpenCredits()
    {
        MenuPanel.SetActive(false);
        LevelPanel.SetActive(false);
        HowToPlayPanel.SetActive(false);
        OptionsPanel.SetActive(false);
        Credits.SetActive(true);
    }

    public void HowToPlay()
    {
        MenuPanel.SetActive(false);
        LevelPanel.SetActive(false);
        HowToPlayPanel.SetActive(true);
        ExitPanel.SetActive(false);
        Credits.SetActive(false);
    }

    public void OpenExit()
    {
        MenuPanel.SetActive(false);
        LevelPanel.SetActive(false);
        HowToPlayPanel.SetActive(false);
        ExitPanel.SetActive(true);
        Credits.SetActive(false);
    }

    public void BackToMenu()
    {
        MenuPanel.SetActive(true);
        LevelPanel.SetActive(false);
        OptionsPanel.SetActive(false);
        HowToPlayPanel.SetActive(false);
        ExitPanel.SetActive(false);
        Credits.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    IEnumerator FadeMusic()
    {
        FindObjectOfType<FadingScene>().fadeDirection = 1;
        while (music.volume > .1F)
        {
            music.volume = Mathf.Lerp(music.volume, 0F, Time.deltaTime);
            yield return 0;
        }
        music.volume = 0;

        switch (selectedScene)
        {
            case 1:
                SceneManager.LoadScene("Level_One");
                break;
            case 2:
                SceneManager.LoadScene("Level_Two");
                break;
            case 3:
                SceneManager.LoadScene("Level_Three");
                break;
            default:
                break;
        }
    }

    public void PlayHoverAudio(int audio)
    {
        if(audio < ButtonsHoverAudios.Length)
        {
            music.PlayOneShot(ButtonsHoverAudios[audio]);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnManager : MonoBehaviour {

    public int NumRows;
    public int NumCols;

    public GameObject SecurityPrefab;
    public GameObject[] CrowdPrefabs;
    public GameObject[,] AllPeople;
    List<PersonBehaviour> SurfingPeople = new List<PersonBehaviour>();
    private int CrowdSize;

    private Vector3 SpawnPos;
    private float gap;

    private static SpawnManager m_Instance;
    public static SpawnManager GetInstance()
    {
        if(!m_Instance)
        {
            m_Instance = FindObjectOfType<SpawnManager>();
            if(!m_Instance)
            {
                GameObject newSpawnManager = new GameObject("SpawnManager");
                m_Instance = newSpawnManager.AddComponent<SpawnManager>();
            }
        }
        return m_Instance;
    }

	// initializing the crowd
	void Awake () {
        CrowdSize = CrowdPrefabs.Length;
        SpawnPos = gameObject.transform.position;
        AllPeople = new GameObject[NumRows, NumCols];
        gap = 0.65f * 1.25f;
        for(int i = 0; i < NumRows; i++)
        {
            for(int j = 0; j < NumCols; j++)
            {
                //select a random prefab
                int person = Random.Range(0, CrowdSize );
                //instantiate the selected prefab
                GameObject go = Instantiate(CrowdPrefabs[person], new Vector3(SpawnPos.x + j * gap, SpawnPos.y, SpawnPos.z + i * gap), Quaternion.Euler(0.0f, -90.0f, 0.0f));
                go.transform.localScale *= 1.25f;
                //go.GetComponent<CapsuleCollider>().radius *= 1.25f;

                //define the game manager to be the parent of these objects
                go.transform.parent = gameObject.transform;

                //naming the new spawned objects with rows and columsn with D2 (two decimals)
                go.name = "Crowd " + i.ToString("D2") + j.ToString("D2");

                // saving the reference for each person in the crowd
                AllPeople[i, j] = go;
            }
        }
	}

    public void SpawnSecurity()
    {
        Vector3 SpawnLoc = GameObject.FindGameObjectWithTag("SecuritySpawnPosition").transform.position;
        Instantiate(SecurityPrefab,SpawnLoc, Quaternion.identity);
    }

    public void CreateNewPerson(int row, int col)
    {
        StartCoroutine(CreateNewCoroutine(row, col));
    }

    IEnumerator CreateNewCoroutine(int row, int col)
    {
        yield return new WaitForSeconds(0.5f);

        int newPersonType = Random.Range(0, CrowdSize);

        if (AllPeople[row, col])
        {
            int currPersonType = AllPeople[row, col].GetComponent<PersonBehaviour>().Type;

            while (newPersonType == currPersonType)
            {
                newPersonType = Random.Range(0, CrowdSize);
            }
        }

        GameObject go = Instantiate(CrowdPrefabs[newPersonType], new Vector3(SpawnPos.x + col * gap, SpawnPos.y, SpawnPos.z + row * gap), Quaternion.identity);
        AllPeople[row, col] = go;
    }

    public void AddSurfingPeople(PersonBehaviour SurfingPerson)
    {
        SurfingPeople.Add(SurfingPerson);
        
        if(SceneManager.GetActiveScene().name != "Level_One")
        { 
            int securityChance = Random.Range(0, 100);

            if (securityChance <= 50)
                SpawnSecurity();
        }
    }

    public void RemoveSurfingPeople(PersonBehaviour NoLongerSurfingPerson)
    {
        SurfingPeople.Remove(NoLongerSurfingPerson);
        SurfingPeople.TrimExcess();
    }

    public List<PersonBehaviour> GetSurfingPeople()
    {
        return SurfingPeople;
    }
}

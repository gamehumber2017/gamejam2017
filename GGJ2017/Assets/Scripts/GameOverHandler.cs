﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverHandler : MonoBehaviour {

    public Animator AnimatorController;

    private Speaker speaker;

    private void OnEnable()
    {
        AnimatorController.SetTrigger("GameOver");

        InputManager.GetInstance().SetEnabledInput(false);
        speaker = FindObjectOfType<Speaker>();

        if(speaker && speaker.GetComponent<AudioSource>().isPlaying)
        {
            speaker.GetComponent<AudioSource>().Pause();
        }
    }

    public void RestartLevel()
    {
        InputManager.GetInstance().SetEnabledInput(true);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void QuitLevel()
    {
        InputManager.GetInstance().SetEnabledInput(true);
        SceneManager.LoadScene("MainMenu");
    }
}

﻿using UnityEngine.EventSystems;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MousePointerEventController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public AudioSource audioSource;

    public void OnPointerEnter(PointerEventData eventData)
    {
        gameObject.transform.GetChild(0).GetComponent<Text>().color = new Color(1.0f, 0.0f, 0.0f, 1.0f);
        audioSource.Play();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        gameObject.transform.GetChild(0).GetComponent<Text>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonBubble : MonoBehaviour {


    Sprite NeedsImage;
    SpriteRenderer spriteRenderer;
    Camera PlayerCamera;

    Color OriginalColor;
    Color Flashing;

    private float flashRate;
    private float flashTimer;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (!spriteRenderer)
            spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
        PlayerCamera = Camera.main;
        flashRate = 0.1f;
        OriginalColor = spriteRenderer.color;
    }

    public void LoadNewImage(Sprite NewNeedImage, Color FlashingColor)
    {
        NeedsImage = NewNeedImage;
        spriteRenderer.sprite = NeedsImage;
        Flashing = FlashingColor;
    }

    // Use this for initialization
    void Start ()
    {
		
	}

    private void FixedUpdate()
    {

    }
    void FlashBubbleColor()
    {
        if(spriteRenderer.color == OriginalColor)
        {
            spriteRenderer.color = Flashing;
        }
        else 
        {
            spriteRenderer.color = OriginalColor;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        Vector3 lookDir = (PlayerCamera.transform.position - gameObject.transform.position).normalized;
        transform.rotation = Quaternion.LookRotation(lookDir,Vector3.up);
        transform.position = transform.parent.position + Vector3.up * 3.0f;

        if (flashTimer > flashRate)
        {
            FlashBubbleColor();
            flashTimer = 0;
        }
        else
            flashTimer += Time.deltaTime;

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityBehaviour : MonoBehaviour {

    // Use this for initialization
    PersonBehaviour Target;
    Rigidbody rb;
    Collider collider;

    public float Speed;
    Vector3 direction;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();
        Speed = 2.5f;
    }

    void Start ()
    {
       List<PersonBehaviour> SurfingPeople =  SpawnManager.GetInstance().GetSurfingPeople();

	}

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (Target)
            Chase();
        else
        {
            List<PersonBehaviour> SurfingPeople = SpawnManager.GetInstance().GetSurfingPeople();

            PersonBehaviour newTarget = SurfingPeople[Random.Range(0, SurfingPeople.Count)];
            if (newTarget)
                Target = newTarget;
        }
    }
    void Update ()
    {

	}

    void Chase()
    {
        direction = Vector3.Lerp(direction, (Target.transform.position - gameObject.transform.position).normalized, 0.25f);
        direction.y = 0;
        rb.velocity = direction * Speed;
    }

    void CaughtPerson()
    {
        EnergyManager.GetInstance().ChangeEnergy(-0.1f);
        SpawnManager.GetInstance().RemoveSurfingPeople(Target);
        CrowdManager.GetInstance().RemovePersonWithNeedsCount();
        Destroy(Target.gameObject);
        Destroy(gameObject);
    }

    public void HitByWave(float WaveStrenth, Vector3 WaveDirection)
    {
        Speed /= 2;
        direction += Quaternion.Euler(0, Random.Range(-90, 90), 0) * WaveDirection;
    }
    public void HitByWaveStopped()
    {
        Speed *= 2;
    }

    private void OnCollisionEnter(Collision collision)
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        PersonBehaviour person = other.gameObject.GetComponent<PersonBehaviour>();
        if (person == Target)
        {
            CaughtPerson();

        }
    }

}

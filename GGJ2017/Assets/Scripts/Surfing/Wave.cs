﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour {


    BoxCollider2D Collider;
    Rigidbody RB;

    public float WaveSpeed;
    public float WaveForce;
    public float LifeTime;



    private void Awake()
    {
        Collider = GetComponent<BoxCollider2D>();
        RB = GetComponent<Rigidbody>();
    }

	// Use this for initialization
	void Start ()
    {
	}

    private void OnTriggerEnter(Collider other)
    {

        PersonBehaviour person = other.GetComponent<PersonBehaviour>();
        SecurityBehaviour Sec = other.GetComponent<SecurityBehaviour>();
        if(person != null)
        {
            person.DoAWave(WaveForce, transform.forward);
        }
        else if(Sec)
        {
            Sec.HitByWave(WaveForce, transform.forward);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        PersonBehaviour person = other.GetComponent<PersonBehaviour>();
        SecurityBehaviour Sec = other.GetComponent<SecurityBehaviour>();
        if (person != null)
        {

            //if (person.bIsRidingCrowd)
            //{
            //    person.GetComponent<Rigidbody>().velocity = Vector3.zero;
            //}
        }
        else if (Sec)
        {
            Sec.HitByWaveStopped();
        }
    }


    void FixedUpdate()
    {
        RB.MovePosition(RB.position + (transform.forward * WaveSpeed));
    }

    // Update is called once per frame
    void Update () {
        if (LifeTime <= 0)
            Destroy(gameObject);
        else
            LifeTime -= Time.deltaTime;
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveGenerator : MonoBehaviour {

    GameObject LeftWaveSpawnPosition;
    GameObject RightWaveSpawnPosition;
    GameObject UpWaveSpawnPosition;
    GameObject DownWaveSpawnPosition;

    public AudioSource waveSource;
    public AudioClip waveSound;

    float WaveSpawnRate;
    float WaveSPawnTimer;

    public GameObject WavePrefab;

    public enum WaveDirection
    {
        UPWARDS,
        DOWNWARDS,
        LEFT,
        RIGHT
    }

    private static WaveGenerator m_Instance;

    public static WaveGenerator GetInstance()
    {
        if(m_Instance == null)
        {
            m_Instance = FindObjectOfType<WaveGenerator>();

            if(!m_Instance)
            {
                GameObject newWaveGenerator = new GameObject("WaveGenerator");
                newWaveGenerator.tag = "WaveGenerator";
                m_Instance = newWaveGenerator.AddComponent<WaveGenerator>();
            }
        }

        return m_Instance;

    }

    public void CreateWave(WaveDirection Direction, float Speed, float Force, float LifeTime)
    {
        if (WavePrefab != null)
        {
            Wave newWave;
            switch (Direction)
            {
                case WaveDirection.UPWARDS:
                    newWave = Instantiate(WavePrefab).GetComponent<Wave>();
                    newWave.WaveForce = Force;
                    newWave.WaveSpeed = Speed;
                    newWave.LifeTime = LifeTime;
                    newWave.transform.position = UpWaveSpawnPosition.transform.position;
                    newWave.transform.rotation = UpWaveSpawnPosition.transform.rotation;
                    break;

                case WaveDirection.LEFT:
                    newWave = Instantiate(WavePrefab).GetComponent<Wave>();
                    newWave.WaveForce = Force;
                    newWave.WaveSpeed = Speed;
                    newWave.LifeTime = LifeTime;
                    newWave.transform.position = LeftWaveSpawnPosition.transform.position;
                    newWave.transform.rotation = LeftWaveSpawnPosition.transform.rotation;
                    break;

                case WaveDirection.DOWNWARDS:
                    newWave = Instantiate(WavePrefab).GetComponent<Wave>();
                    newWave.WaveForce = Force;
                    newWave.WaveSpeed = Speed;
                    newWave.LifeTime = LifeTime;
                    newWave.transform.position = DownWaveSpawnPosition.transform.position;
                    newWave.transform.rotation = DownWaveSpawnPosition.transform.rotation;
                    break;

                case WaveDirection.RIGHT:
                    newWave = Instantiate(WavePrefab).GetComponent<Wave>();
                    newWave.WaveForce = Force;
                    newWave.WaveSpeed = Speed;
                    newWave.LifeTime = LifeTime;
                    newWave.transform.position = RightWaveSpawnPosition.transform.position;
                    newWave.transform.rotation = RightWaveSpawnPosition.transform.rotation;
                    break;
                default:
                    break;
            }
            EnergyManager.GetInstance().ChangeEnergy(-0.05f);
            waveSource.PlayOneShot(waveSound);
        }
    }


    private void Awake()
    {
        LeftWaveSpawnPosition = GameObject.FindGameObjectWithTag("LeftWaveSpawnPosition");
        RightWaveSpawnPosition = GameObject.FindGameObjectWithTag("RightWaveSpawnPosition");
        UpWaveSpawnPosition = GameObject.FindGameObjectWithTag("UpWaveSpawnPosition");
        DownWaveSpawnPosition = GameObject.FindGameObjectWithTag("DownWaveSpawnPosition");
        WaveSpawnRate = 2.0f;
        //DownWaveSpawnPosition = GameObject.FindGameObjectWithTag("DownWaveSpawnPosition").transform.position;
    }

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        //if (WaveSPawnTimer >= WaveSpawnRate/10)
        //{
        //    CreateWave(WaveDirection.UPWARDS, 0.25f, 1.0f, WaveSpawnRate);
        //    //CreateWave(WaveDirection.LEFT, 0.25f, 1.0f, WaveSpawnRate);
        //    WaveSPawnTimer = 0;
        //}
        //else
        //    WaveSPawnTimer += Time.deltaTime;
	}
}

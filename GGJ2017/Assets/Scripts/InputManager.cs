﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class InputManager : MonoBehaviour {

    // defined in the inspector
    public SpawnManager spawnManager;
    public float PushForce = 7.0f;

    private bool bActiveInputs;

    public GameObject InGameMenu;
    public bool InGameMenuToggle;
    public bool HasLost;

    public AudioSource BandAudio;

    private static InputManager m_Instance;
    public static InputManager GetInstance()
    {
        if (!m_Instance)
        {
            m_Instance = FindObjectOfType<InputManager>();
            if (!m_Instance)
            {
                GameObject newSpawnManager = new GameObject("InputManager");
                m_Instance = newSpawnManager.AddComponent<InputManager>();
            }
        }
        return m_Instance;
    }


    private void Awake()
    {
        bActiveInputs = true;
        InGameMenuToggle = false;
        HasLost = false;
    }

    private void Start()
    {
    }
    // Update is called once per frame
    void Update () {

        if(bActiveInputs)
        {
            if (Input.GetKeyDown(KeyCode.O))
            {
                int r = Random.Range(0, SpawnManager.GetInstance().NumRows);
                int c = Random.Range(0, SpawnManager.GetInstance().NumCols);
                GameObject currentPerson = SpawnManager.GetInstance().AllPeople[r, c];

                if (currentPerson)
                {
                    currentPerson.GetComponent<PersonBehaviour>().StartMosh();
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                EscapePressed();
            }

            if (Input.GetAxis("JHorizontal") < 0)
            {
                WaveGenerator.GetInstance().CreateWave(WaveGenerator.WaveDirection.LEFT, 0.1f, 7.0f, 10.0f);
            }

            if (Input.GetAxis("JHorizontal") > 0)
            {
                WaveGenerator.GetInstance().CreateWave(WaveGenerator.WaveDirection.RIGHT, 0.1f, 7.0f, 10.0f);
            }

            if (Input.GetKeyDown(KeyCode.D))
            {
                WaveGenerator.GetInstance().CreateWave(WaveGenerator.WaveDirection.UPWARDS, 0.1f, 6.0f, 10.0f);
            }

            if (Input.GetKeyDown(KeyCode.W))
            {
                WaveGenerator.GetInstance().CreateWave(WaveGenerator.WaveDirection.LEFT, 0.1f, 7.0f, 10.0f);
            }

            if (Input.GetKeyDown(KeyCode.A))
            {
                WaveGenerator.GetInstance().CreateWave(WaveGenerator.WaveDirection.DOWNWARDS, 0.1f, 7.0f, 10.0f);
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                WaveGenerator.GetInstance().CreateWave(WaveGenerator.WaveDirection.RIGHT, 0.1f, 7.0f, 10.0f);
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                SpawnManager.GetInstance().SpawnSecurity();
            }
        }
    }

    public void SetEnabledInput(bool b)
    {
        bActiveInputs = b;
    }

    private void EscapePressed()
    {
        if (!InGameMenuToggle)
        {
            InGameMenuToggle = true;
            Time.timeScale = 0;

            BandAudio.Pause();
            InGameMenu.SetActive(true);
        }
        else
        {
            InGameMenuToggle = false;
            Time.timeScale = 1;

            BandAudio.Play();
            InGameMenu.SetActive(false);
        }
    }

    public void ResumeGame()
    {
        EscapePressed();
    }

    public void QuitGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");

    }
}
